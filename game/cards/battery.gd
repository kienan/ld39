extends "card_base.gd"

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
export(int) var  restore_strength


func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func _proc_effects():
	self.game.update_energy(self.restore_strength)