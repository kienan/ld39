extends "card_base.gd"

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func _card_help():
	return "Select one area and one card. The card will be destroyed, and the area will be repaired by 1. Arguments: %d / %d. Press enter to confirm, escape to cancel" % [self.arguments.size(), self.args_required_to_play]

func validate_args():
	if self.arguments.size() == 2:
		var a = self.arguments[0]
		var b = self.arguments[1]
		if (a.is_in_group('areas') && b.is_in_group('cards')):
			return true
		if (a.is_in_group('cards') && b.is_in_group('areas')):
			return true
		return false
	return false

func validate_argument(node):
	if node.is_in_group('areas') || node.is_in_group('cards'):
		return true
	return false
	
func _proc_effects():
	for a in self.arguments:
		if a.is_in_group('cards'):
			self.game.destroy_card(a)
		if a.is_in_group('areas'):
			a.set_health(a.health + 1)