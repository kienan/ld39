extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass



func _on_area_base_game_area_health_changed(old, new, max_health):
	get_node('Label').set_text('%d / %d' %[new, max_health])


func _on_reactor_game_area_health_changed(old, new, max_health):
	self._on_area_base_game_area_health_changed(old, new, max_health)
