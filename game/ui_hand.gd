extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass


func _on_game_game_hand_changed(count):
	var game = get_node("/root/game")
	if not game:
		return
	var origin = self.get_global_pos()
	var offset = Vector2(25, 39)
	var hand = game.get('hand')
	print(hand.size())
	for i in range(0, hand.size()):
		var card = hand[i]
		if not card:
			continue
		card.set_global_pos(origin + offset + Vector2(i*100, 0))
		card.show()
	var cards = game.get_tree().get_nodes_in_group('cards')
	for c in cards:
		var i = hand.find(c)
		if i == -1:
			c.previous_location = Vector2(-200, -200)
			c.hide()