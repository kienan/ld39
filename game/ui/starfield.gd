extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
export var star_count = 128
var star_textures = [
  preload("stars/star0.tex")
]

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	randomize()
	var bg = get_node('bg')
	var origin = bg.get_pos()
	var  corner = bg.get_item_rect().end * bg.get_scale()
	for i in range(1, star_count):
		var n = Sprite.new()
		n.set_texture(star_textures[rand_range(0, star_textures.size()-1)])
		var x = randf() * (corner.x - origin.x)
		var y = randf() * (corner.y - origin.y)
		n.set_pos(Vector2(x, y))
		n.set_modulate(Color(randf(), randf(), randf()))
		n.show()
		add_child(n)