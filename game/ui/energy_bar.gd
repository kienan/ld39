extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func _on_game_game_energy_changed(new_value):
	get_node("ProgressBar").set_value(new_value)
	get_node("Label").set_text('%d' % new_value)
