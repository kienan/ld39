extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func _on_game_deck_changed(card_count):
	if card_count <= 0:
		get_node('last_card').hide()
	else:
		get_node('last_card').show()