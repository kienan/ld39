extends Node2D

signal game_area_activation_changed
signal game_area_health_changed
signal game_area_clicked

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
export(int) var max_health = 3
export(int) var health = 3
export(bool) var activated = false
export(int) var energy_cost = 0
export(String) var display_name = ''
var loot_table = {}

func set_health(health):
	if health > self.max_health:
		health = self.max_health
	if (health == self.health):
		return
	var old_health = self.health
	self.health = health
	if self.health < 0:
		self.health = 0
	if self.health == 0:
		get_node('CheckButton').set_disabled(true)
	if self.health > 0:
		get_node('CheckButton').set_disabled(false)
	emit_signal('game_area_health_changed', old_health, health, self.max_health)

func set_activate(activate):
	if self.activated == activate:
		return
	self.activated = activate
	emit_signal('game_area_activation_changed', self, activate)

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	get_node('Label').set_text('Cost: %d energy' % self.energy_cost)
	get_node('display_name').set_text(self.display_name)
	emit_signal('game_area_health_changed', self.health, self.health, self.max_health)
	if self.health <= 0:
		get_node('CheckButton').set_disabled(true)
	emit_signal('game_area_activation_changed', self, self.activated)


func _on_Area2D_input_event(viewport, event, shape_idx):
	if event.type == InputEvent.MOUSE_BUTTON && event.button_index == BUTTON_LEFT && event.pressed:
		emit_signal('game_area_clicked', self)


func _on_CheckButton_toggled( pressed ):
	self.set_activate(pressed)

func set_hilight(x):
	if x:
		get_node('Sprite').set_modulate(Color(0, 1, 0, 1))
	else:
		get_node('Sprite').set_modulate(Color(1, 1, 1, 1))