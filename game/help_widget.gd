extends TextureButton

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var widget_shown = false
var current_texture_idx = 0
var textures = [
	load('res://ui/help/help_1.tex'),
	load('res://ui/help/help_2.tex'),
	load('res://ui/help/help_3.tex'),
	load('res://ui/help/help_4.tex'),
]

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_process_unhandled_key_input(true)

func _unhandled_key_input(event):
	if not self.widget_shown:
		return
	if event.type == InputEvent.KEY && event.scancode == KEY_ESCAPE:
		self.show_widget(false)
		get_tree().set_input_as_handled()

func show_widget(enable):
	self.widget_shown = enable
	if enable:
		self.set_widget_page(self.current_texture_idx)
		get_node('Container').show()
	else:
		get_node('Container').hide()
		self.set_widget_page(0)

func _on_icon_pressed():
	self.show_widget(!self.widget_shown)

func set_widget_page(idx):
	if idx < 0:
		return
	if idx >= self.textures.size():
		return
	get_node('Container/TextureFrame').set_texture(self.textures[idx])
	get_node('Container/Label').set_text("Page %d / %d" % [idx + 1, self.textures.size() ])
	if idx == 0:
		get_node('Container/Previous').set_disabled(true)
	else:
		get_node('Container/Previous').set_disabled(false)
	if idx == self.textures.size() - 1:
		get_node('Container/Next').set_disabled(true)
	else:
		get_node('Container/Next').set_disabled(false)
	self.current_texture_idx = idx

func _on_Next_pressed():
	self.set_widget_page(self.current_texture_idx + 1)

func _on_Previous_pressed():
	self.set_widget_page(self.current_texture_idx - 1)
